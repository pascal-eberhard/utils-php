# Some helpers and tools

## Description

Stuff not clearly matching to another of my libraries.

### [Change log](resources/docs/changelog.md)

### License

The MIT License, see [License File](LICENSE.md).

## Installation

Via composer:

```bash
composer require pascal-eberhard/utils-php
```

## Some shell commands

```bash
# All checks
composer checks

# One at a time, see composer.json "scripts"
#composer [insert composer script label, without @ prefix]
```
