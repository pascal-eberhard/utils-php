# Change log

## Version 1.4

```yaml
checks:
  # @todo Currently out of build time
  bitBucket: ""
  local:
    composerChecksPassed: true
    php: "7.2.1"
    operatingSystem:
      extry: "Git bash"
      name: "Windows 7"
repository:
  commit: "https://bitbucket.org/pascal-eberhard/utils-php/commits/f3642b8eb7966f41e2b86824b54faddee4ea5125"
```

Add some helper classes for unit tests.

## Version 1.3

```yaml
checks:
  # @todo Currently out of build time
  bitBucket: ""
  local:
    composerChecksPassed: true
    php: "7.2.1"
    operatingSystem:
      extry: "Git bash"
      name: "Windows 7"
repository:
  commit: "https://bitbucket.org/pascal-eberhard/utils-php/commits/87bf91e463fdf327850ef586c1357e2e51b966c3"
```

Deleted previous versions.
Change from three part to two part version number.
