<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\UnitTests;

/**
 * Unit tests for classes based at symfony service concept
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait WithServiceTrait
{

    /**
     * @var object|null
     */
    private $service = null;

    /**
     * Create service instance, different per sub class
     *
     * @return object
     */
    abstract protected function createService();

    /**
     * Get service
     * A bit caching
     * Overwrite in sub class to set the correct return annotation
     *
     * @return object
     */
    protected function getService()
    {
        return $this->getServiceBase();
    }

    /**
     * Get service
     * A bit caching
     *
     * @return object
     */
    final protected function getServiceBase()
    {
        if (null !== $this->service) {
            return $this->service;
        }

        $this->service = $this->createService();

        return $this->service;
    }
}
