<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\UnitTests;

/**
 * Simple value object for objects to test constructor parameter
 * For easier test data handling
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface ObjectInputInterface
{

    /**
     * Create the related object with the constructor parameter
     *
     * @return object
     */
    public function toObject();
}
