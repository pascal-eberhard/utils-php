<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\UnitTests;

use PEPrograms\Utils\ClassAndObject\With\ToArray;

/**
 * Simple value object for objects to test constructor parameter
 * For easier test data handling
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface ObjectOutputInterface extends ToArray\TheInterface
{
}
