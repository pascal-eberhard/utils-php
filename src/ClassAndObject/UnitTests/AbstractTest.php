<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\ClassAndObject\UnitTests;

use PHPUnit\Framework\TestCase;

/**
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * @codingStandardsIgnoreStart
 * Shell: (vendor/bin/phpunit [relative sub class path])
 * Shell/testClassPaths: (vendor/bin/phpunit [relative sub class path] --filter '/::testClassPaths\b/')
 * @codingStandardsIgnoreEnd
 * @ \b, else all tests matching "testX*" would be executed
 */
abstract class AbstractTest extends TestCase
{

    /**
     * @return array, per item:
     * param string $classPath Expected
     * param object $instance
     * param string $note About the test case
     * @see self::testClassPaths()
     */
    abstract public function dataClassPaths(): array;

    /**
     * @covers ::__construct
     * @covers ::get
     * @dataProvider dataClassPaths
     *
     * @param string $classPath Expected
     * @param object $instance
     * @param string $note About the test case
     * @throws \InvalidArgumentException
     *
     * Shell: (vendor/bin/phpunit [relative sub class path] --filter '/::testClassPaths\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    final public function testClassPaths(string $classPath, $instance, string $note)
    {
        if ('' == $classPath) {
            throw new \InvalidArgumentException('Empty string not allowed for $classPath');
        } elseif (!\is_object($instance)) {
            throw new \InvalidArgumentException('Wrong type for $instance, must be object not '
                . \gettype($instance));
        }

        $this->assertEquals($classPath, \get_class($instance), $note);
    }
}
