<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\ClassAndObject\With\GetInstanceSelf;

/**
 * Get instance for final classes with default constructor
 * For easier handling. It is shorter and with new you have to enclose with brackets to directly use the return value
 * &lt;code&gt;X::y()&lt;/code&gt; vs.  &lt;code&gt;(new X())->y()&lt;/code&gt;
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface TheInterface
{

    /**
     * @return self
     */
    public static function get();
}
