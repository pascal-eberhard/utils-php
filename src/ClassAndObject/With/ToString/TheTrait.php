<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\ClassAndObject\With\ToString;

/**
 * Return string representation of object data
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see <https://www.php.net/manual/de/language.oop5.magic.php#object.tostring>
 */
trait TheTrait
{

    /**
     * To string
     *
     * @return string
     */
    public function __toString(): string
    {
        return '';
    }

    /**
     * To string
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->__toString();
    }
}
