<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\ClassAndObject\With\ToArray;

/**
 * For simple data output, e.g. for debug
 * JsonSerializable would be nice, but therefore PHP ext-json must be installed, but this package needs no JSON
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface TheInterface
{

    /**
     * @return array
     */
    public function toArray(): array;
}
