<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\ClassAndObject\With\ParentInstance;

/**
 * You can only use the PHP parent keyword if there is a parent instance
 * That means you can only write parent related methods if there really is a parent instance
 * Let's improve this a bit
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait TheTrait
{

    /**
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function hasParentInstance(): bool
    {
        $result = \get_parent_class($this);
        if (!\is_string($result)) {
            return false;
        } elseif ('' != $result) {
            return true;
        }

        throw new \UnexpectedValueException('PHP:get_parent_class() returned empty string for class '
            . \get_class($this));
    }
}
