<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\ParentInstance;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\Utils\Tests\ClassAndObject\With\ParentInstance\WithNoParentClass
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * @codingStandardsIgnoreStart
 * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ParentInstance/TheTest.php)
 * @codingStandardsIgnoreEnd
 * @ \b, else all tests matching "testX*" would be executed
 */
class TheTest extends TestCase
{

    /**
     * @return array, per item:
     * param bool $expected
     * param bool $current
     * param string $note About the test case
     * @see self::testHasParentInstance()
     */
    public function dataHasParentInstance(): array
    {
        return [
            [false, (new NoParentClass())->hasParentInstance(), NoParentClass::class],
            [true, (new HasParentClass())->hasParentInstance(), HasParentClass::class],
        ];
    }

    /**
     * @covers ::hasParentInstance
     * @dataProvider dataHasParentInstance
     *
     * @param bool $expected
     * @param bool $current
     * @param string $note About the test case

     * @codingStandardsIgnoreStart
     * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ParentInstance/TheTest.php --filter '/::testHasParentInstance\b/')
     * @codingStandardsIgnoreEnd
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testHasParentInstance(bool $expected, bool $current, string $note)
    {
        $this->assertEquals($expected, $current, $note);
    }
}
