<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\GetInstanceStatic;

use PEPrograms\Utils\ClassAndObject\UnitTests\AbstractTest;

/**
 * @coversDefaultClass \PEPrograms\Utils\Tests\ClassAndObject\With\GetInstanceStatic\TestClass
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 *
 * @codingStandardsIgnoreStart
 * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/GetInstanceStatic/TheTest.php)
 * Shell/testClassPaths: (vendor/bin/phpunit tests/ClassAndObject/With/GetInstanceStatic/TheTest.php --filter '/::testClassPaths\b/')
 * @codingStandardsIgnoreEnd
 * @ \b, else all tests matching "testX*" would be executed
 */
class TheTest extends AbstractTest
{

    /**
     * @return array, per item:
     * param string $classPath Expected
     * param object $instance
     * param string $note About the test case
     * @see self::testClassPaths()
     */
    public function dataClassPaths(): array
    {
        return [
            [SubClass::class, new SubClass(), 'By SubClass::__construct()'],
            [SubClass::class, SubClass::get(), 'By SubClass::get()'],
            [TestClass::class, new TestClass(), 'By TestClass::__construct()'],
            [TestClass::class, TestClass::get(), 'By TestClass::get()'],
        ];
    }
}
