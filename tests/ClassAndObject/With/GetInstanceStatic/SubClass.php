<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\GetInstanceStatic;

/**
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class SubClass extends TestClass
{
}
