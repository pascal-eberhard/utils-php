<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\ToString;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\Utils\Tests\ClassAndObject\With\ToString\TestClass
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ToString/TheTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class TheTest extends TestCase
{

    /**
     * @return array, per item:
     * param string $expected
     * param string $current
     * param string $note About the test case
     * @see self::testToString()
     */
    public function dataToString(): array
    {
        return [
            ['', (new TestClass())->__toString(), '::__toString()'],
            ['', (new TestClass())->toString(), '::toString()'],
        ];
    }

    /**
     * @covers ::__toString
     * @covers ::toString
     * @dataProvider dataToString
     *
     * @param string $expected
     * @param string $current
     * @param string $note About the test case

     * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ToString/TheTest.php --filter '/::testToString\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testToString(string $expected, string $current, string $note)
    {
        $this->assertEquals($expected, $current, $note);
    }
}
