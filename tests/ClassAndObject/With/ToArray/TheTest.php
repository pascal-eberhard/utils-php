<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\ToArray;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\Utils\Tests\ClassAndObject\With\ToArray\TestClass
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ToArray/TheTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class TheTest extends TestCase
{

    /**
     * @covers ::toArray
     *
     * Shell: (vendor/bin/phpunit tests/ClassAndObject/With/ToArray/TheTest.php --filter '/::testToArray\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testToArray()
    {
        $this->assertEquals([], (new TestClass())->toArray());
    }
}
