<?php declare(strict_types=1);

/*
 * This file is part of the utils-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\Utils\Tests\ClassAndObject\With\ToArray;

use PEPrograms\Utils\ClassAndObject\With\ToArray as ToTest;

/**
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
final class TestClass implements ToTest\TheInterface
{
    use ToTest\TheTrait;
}
